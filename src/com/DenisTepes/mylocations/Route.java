package com.DenisTepes.mylocations;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import android.text.method.DateTimeKeyListener;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Route {
	@Expose
	@SerializedName("id")
	public int id;
	@Expose
	@SerializedName("userid")
	public int userid;
	@Expose
	@SerializedName("name")
	public String name;
	@Expose
	@SerializedName("description")
	public String description;
	@Expose
	@SerializedName("add_time")
	public long add_time;
	@Expose
	@SerializedName("points")
	public ArrayList<Points> points;
	
	public Route() {
		super();
	}
	
	public Route(int id, int userid, String name, String description, long add_time) {
		super();
		this.id = id;
		this.userid = userid;
		this.name = name;
		this.description = description;
		this.add_time = add_time;
		this.points = new ArrayList<Points>();
	}
	
	public void addPoint(Points point){
		points.add(point);
	}
	
	public ArrayList<Points> getPoints(){
		return points;
	}
	
	public String getAddTime(){
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		Date dt = new Date(add_time * 1000);
		return df.format(dt);
	}
	
	public void setVisitedPoint(int id){
		for(int i = 0; i < points.size(); i++)
		{
			if(points.get(i).id == id)
				points.get(i).visited = true;
		}
	}
}

package com.DenisTepes.mylocations;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class User {
	@Expose
	@SerializedName("username")
	public String username;
	@Expose
	@SerializedName("password")
	public String password;
	
	public User() {
		super();
	}
	
	public User(String username, String password) {
		super();
		this.username = username;
		this.password = password;
	}
}

package com.DenisTepes.mylocations;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DateFormat;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;


import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.util.Base64;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.Toast;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ExpandableListView.OnGroupClickListener;
import android.view.View.OnClickListener;

public class SelectionActivity extends Activity implements OnClickListener {

	private ExpandableListView eListPrikaz;
	private ProgressDialog progressDialog;
	
	public Context context = this;
	public Dialog dialog = null;

	public ArrayList<Route> list;
	public RouteAdapter addapt;
	public ApplicationData app;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		this.setTitle("Moje poti");
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_selection);
		
		
		list = new ArrayList<Route>();
		
		app = (ApplicationData) getApplication();
		
		nalozi();
	}
	
	//our child listener
	 private OnChildClickListener myListItemClicked =  new OnChildClickListener() {
	 
		 public boolean onChildClick(ExpandableListView parent, View v,
				 int groupPosition, int childPosition, long id) {
	    
			 //get the group header
			 Route route = list.get(groupPosition);
			 //get the child info
			 Points point =  route.getPoints().get(childPosition);
			 //Toast.makeText(getBaseContext(), "Child on Header dasd asdas", 
			 //		 Toast.LENGTH_LONG).show();
			 
			 return false;
		 }
	 };
	 
	 public void onClick(View v) {
		 
		 
	 }
	 
	 //our group listener
	 private OnGroupClickListener myListGroupClicked =  new OnGroupClickListener() {
	 
		 public boolean onGroupClick(ExpandableListView parent, View v,
				 int groupPosition, long id) {
	    
			 //get the group header
			 Route route = list.get(groupPosition);
			 //display it or do something with it
			 //Toast.makeText(getBaseContext(), "Child on Header ", 
			 //		 Toast.LENGTH_LONG).show();
	     
			 return false;
		 }
	 };

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.selection, menu);
		return true;
	}
	
	public boolean najdiPoti() {
		try {
			Reader reader = new InputStreamReader(new FileInputStream(Environment.getExternalStorageDirectory() + "/vaja_3/Avtomobili.txt"));
			java.lang.reflect.Type list2 = new TypeToken<ArrayList<Route>>(){}.getType();
			
			Gson gson = new GsonBuilder().setPrettyPrinting().create();
			
			list = (ArrayList<Route>) gson.fromJson(reader, list2);
			reader.close();
			return true;
			
		} catch (IOException e) {
			System.out.println("Error load Team");
		}
		return false;
	}

	public void nalozi(){
		// Get my routes
		new ApiCall().execute(app.user.username, app.user.password);
		
		// Get Shared Routes
		new ApiCallGetSharedRoutes().execute(app.user.username, app.user.password);
	}
	
private class ApiCall extends AsyncTask<String, ArrayList<Route>, ArrayList<Route>>{
		
        @Override
        protected ArrayList<Route> doInBackground(String... da) {
        	try{
				HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/get-routes").openConnection();
				conn.setUseCaches(false);
			   
				conn.setRequestProperty("Authorization", "Basic " +
			           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));
			   
				conn.connect();
			   
				int response = conn.getResponseCode();
				
				String data="";
				if (response == HttpURLConnection.HTTP_OK) {
					InputStream inputStream = conn.getInputStream();
	   
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(inputStream));
					String temp;
					while ((temp = bufferedReader.readLine()) != null) {
						data+=temp;
					}
				}
				
				System.out.println(data);
				
				Gson gson = new GsonBuilder().setDateFormat(DateFormat.LONG).setDateFormat("dd.MM.yyyy").create();
				ArrayList<Route> xxx = (ArrayList<Route>)gson.fromJson(data, new TypeToken<ArrayList<Route>>(){}.getType());
				
				return xxx;
				
			}
			catch (IOException i){
			}
        	
        	return null;
        }
        
        @Override  
        protected void onPreExecute()  
        {  
        	progressDialog = ProgressDialog.show(SelectionActivity.this,"Nalaganje...",  
        		    "Prijavljanje, prosim počakajte...", false, false);
            //Display the progress dialog  
            progressDialog.show();  
        }  
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(ArrayList<Route> result) {
        	progressDialog.dismiss();
        	if(result != null)
        	{
	        		app.routes = result;
	        		list = result;
	        		addapt = new RouteAdapter(SelectionActivity.this, list);

	        		eListPrikaz = (ExpandableListView) findViewById(R.id.eListRoutes);
	        		eListPrikaz.setAdapter(addapt);
	        		  //listener for child row click
	        		eListPrikaz.setOnChildClickListener(myListItemClicked);
	        		  //listener for group heading click
	        		eListPrikaz.setOnGroupClickListener(myListGroupClicked);
	        		
        	}else
	        	Toast.makeText(getApplicationContext(),
	        			"Napaka pri povezavi!",
						Toast.LENGTH_SHORT).show();
        	
        	progressDialog = null;

       }
    }
private class ApiCallGetSharedRoutes extends AsyncTask<String, ArrayList<SharedRoutes>, ArrayList<SharedRoutes>>{
	
    @Override
    protected ArrayList<SharedRoutes> doInBackground(String... da) {
    	try{
			HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/get-shared-routes").openConnection();
			conn.setUseCaches(false);
		   
			conn.setRequestProperty("Authorization", "Basic " +
		           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));
		   
			conn.connect();
		   
			int response = conn.getResponseCode();
			
			String data="";
			if (response == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = conn.getInputStream();
   
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(inputStream));
				String temp;
				while ((temp = bufferedReader.readLine()) != null) {
					data+=temp;
				}
			}
			
			System.out.println(data);
			
			Gson gson = new GsonBuilder().setDateFormat(DateFormat.LONG).setDateFormat("dd.MM.yyyy").create();
			ArrayList<SharedRoutes> xxx = (ArrayList<SharedRoutes>)gson.fromJson(data, new TypeToken<ArrayList<SharedRoutes>>(){}.getType());
			
			return xxx;
			
		}
		catch (IOException i){
		}
    	
    	return null;
    }

    @Override  
    protected void onPreExecute()  
    {  
    }  
    
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(ArrayList<SharedRoutes> result) {
    	
    	if(result != null)
    	{
    		if(result.size() > 0)
	    	{
	    		while(progressDialog != null);
	  
	    		SharedRoutesAdapter addRouteApt = new SharedRoutesAdapter(getApplicationContext(), R.layout.shared_listitems, result, (ApplicationData) getApplication());
	    		
	        	// custom dialog
				dialog = new Dialog(context);
				dialog.setContentView(R.layout.sharedlist);
				dialog.setTitle("Deljene poti");
	
	    		ListView data = (ListView)dialog.findViewById(R.id.listSharedRoutes);
	    		data.setAdapter(addRouteApt);
	 
				dialog.show();
    		}
    	}
   }
}
}

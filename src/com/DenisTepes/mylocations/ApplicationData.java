package com.DenisTepes.mylocations;

import java.util.ArrayList;

import android.app.Application;

public class ApplicationData extends Application{
	public User user;
	//public String ipaddress = "http://192.168.1.124:5000";
	//public String ipaddress = "http://192.168.1.5:5000";
	//public String ipaddress = "http://164.8.7.94:5000";
	//public String ipaddress = "http://192.168.1.7";
	public String ipaddress = "http://obisci-obiscime.rhcloud.com";
	
	public ArrayList<Route> routes;
	public int value;
	
	public Route route = null;
	
	private ArrayList<NotificationID> notificationlist;
	
	public void onCreate() {
        super.onCreate();
        user = null;
        routes = new ArrayList<Route>();
        notificationlist = new ArrayList<ApplicationData.NotificationID>();
    }
	
	public void setUser(String username, String password)
	{
		user = new User(username, password);
	}
	
	public void setUser(User u)
	{
		user = u;
	}
	
	public int setNotificationID(int index)
	{
		int idNumber = 1000;
		
		if(notificationlist.size() > 0)
		{
			idNumber = notificationlist.get(notificationlist.size() - 1).id;
			idNumber++;
		}
		System.out.println("index: " + String.valueOf(idNumber));
		notificationlist.add(new NotificationID(idNumber, index));
		
		return idNumber;
	}
	
	public int getNotificationID(int index)
	{	
		for(int i = 0; i < notificationlist.size(); i++)
		{
			if(notificationlist.get(i).index == index)
			{
				int id = notificationlist.get(i).id;
				notificationlist.remove(i);
				
				return id;
			}
		}
		
		return -1;
	}
	
	public class NotificationID{
		public int id;
		public int index;
		
		public NotificationID(int id, int index)
		{
			this.id = id;
			this.index = index;
		}
	}
}

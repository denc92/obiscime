package com.DenisTepes.mylocations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

import com.google.android.gms.internal.cb;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import android.os.AsyncTask;
import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.Toast;


public class MainActivity extends Activity {
	
	private ApplicationData app;
	private ProgressDialog progressDialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		app = (ApplicationData) getApplication();
		
		Button btn = (Button)findViewById(R.id.btnLogin);
		btn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v){
	        		checkLogin();
	        	}
	       });

		User u = getLogin();
		
		if(u != null)
		{
			//new ApiCall().execute(u.username, u.password);
			EditText txtUsername = (EditText)findViewById(R.id.txtUsername);
			EditText txtPassword = (EditText)findViewById(R.id.txtPassword);
			txtUsername.setText(u.username);
			txtPassword.setText(u.password);
			CheckBox cbRememberMe = (CheckBox)findViewById(R.id.cbRememberMe);
			cbRememberMe.setChecked(true);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void checkLogin()
	{
		EditText txtUsername = (EditText)findViewById(R.id.txtUsername);
		EditText txtPassword = (EditText)findViewById(R.id.txtPassword);
		CheckBox cbRememberMe = (CheckBox)findViewById(R.id.cbRememberMe);

		String username = txtUsername.getText().toString();
		String password = txtPassword.getText().toString();
		if(cbRememberMe.isChecked())
			saveLogin(username, password);
		else
			clearLogin();
		
		
		if(username.length() > 3 && password.length() > 3){
			new ApiCall().execute(username, password);
		}else{
			Toast.makeText(getApplicationContext(),
					"Nepravilni vnos!\nPrekratko uporabni�ko ime ali geslo",
					Toast.LENGTH_SHORT).show();
		}
	}
	public void saveLogin(String username, String password) {
		SharedPreferences sharedPref = this.getSharedPreferences(
				this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		 
		SharedPreferences.Editor editor = sharedPref.edit();
		
		editor.putString("username", username);
		editor.putString("password", password);
		editor.commit();
	}
	
	public User getLogin() {
		SharedPreferences sharedPref = this.getSharedPreferences(
			 this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
		   
		if(sharedPref != null && sharedPref.contains("username") && sharedPref.contains("password")) {
			return new User(sharedPref.getString("username", ""), sharedPref.getString("password", ""));
		}
		  
		return null;
	}
	 
	public void clearLogin() {
		SharedPreferences sharedPref = this.getSharedPreferences(
				this.getString(R.string.preference_file_key), Context.MODE_PRIVATE);
			 
		SharedPreferences.Editor editor = sharedPref.edit();
		
		editor.clear();
		editor.commit();
	}
	
	    
	private class ApiCall extends AsyncTask<String, User, User> {
		
        @Override
        protected User doInBackground(String... da) {
        	try{
				HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/login").openConnection();
				conn.setUseCaches(false);
			   
				conn.setRequestProperty("Authorization", "Basic " +
			           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));
			   
				conn.connect();
			   
				int response = conn.getResponseCode();
				
				String data="";
				if (response == HttpURLConnection.HTTP_OK) {
					InputStream inputStream = conn.getInputStream();
	   
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(inputStream));
					String temp;
					while ((temp = bufferedReader.readLine()) != null) {
						data+=temp;
					}
				}
				//java.lang.reflect.Type list = new TypeToken<User>(){}.getType();
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				User xxx = (User)gson.fromJson(data, User.class);
				
				if(xxx != null)
					if(xxx.password != null)
						xxx.password = da[1];
					
				return xxx;
				
			}
			catch (IOException i){
			}
        	
        	return null;
        }
        
        @Override  
        protected void onPreExecute()  
        {  
        	progressDialog = ProgressDialog.show(MainActivity.this,"Nalaganje...",  
        		    "Prijavljanje, prosim po�akajte...", false, false);
            //Display the progress dialog  
            progressDialog.show();  
        }  
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(User result) {
        	if(result != null)
        	{
        		if(result.username != null){
		        	app.setUser(result);
		        	
		        	System.out.println(app.user.username + ": " + app.user.password);
		        	
	        		Intent i = new Intent(MainActivity.this, SelectionActivity.class);
	        	    startActivity(i);
	        	}else
	        		Toast.makeText(getApplicationContext(),
		        			"Vneseni so bili napa�ni podatki!",
							Toast.LENGTH_SHORT).show();
	        		
        	}else
	        	Toast.makeText(getApplicationContext(),
	        			"Napaka pri povezavi!",
						Toast.LENGTH_SHORT).show();

        	
        	progressDialog.dismiss();
       }
    }

}

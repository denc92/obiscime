package com.DenisTepes.mylocations;

import com.google.gson.annotations.SerializedName;

public class PointData {

	@SerializedName("id")
	public int id;
	@SerializedName("pointid")
	public int pointid;
	@SerializedName("userid")
	public int userid;
	@SerializedName("visited")
	public long visited;

	public PointData(){
		super();
	}
}

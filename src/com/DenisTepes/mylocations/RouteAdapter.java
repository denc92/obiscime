package com.DenisTepes.mylocations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.google.android.gms.auth.GoogleAuthException;

import android.content.Context;
import android.content.Intent;
import android.graphics.Point;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseExpandableListAdapter;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class RouteAdapter extends BaseExpandableListAdapter{

    private Context context;
    private ArrayList<Route> deptList;
     
    public RouteAdapter(Context context, ArrayList<Route> deptList) {
    	this.context = context;
    	this.deptList = deptList;
    }
     
    @Override
    public Object getChild(int groupPosition, int childPosition) {
    	ArrayList<Points> productList = deptList.get(groupPosition).getPoints();
    	return productList.get(childPosition);
    }
    
    @Override
    public long getChildId(int groupPosition, int childPosition) {
    	return childPosition;
    }
    
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, 
      View view, ViewGroup parent) {
      
    	Points points = (Points) getChild(groupPosition, childPosition);
    	if (view == null) {
	    	LayoutInflater infalInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	    	view = infalInflater.inflate(R.layout.child_row, null);
	    }
      
    	 TextView index =(TextView)view.findViewById(R.id.txtPointIndex);
         TextView add_time =(TextView)view.findViewById(R.id.txtPointAddTime);
         TextView description =(TextView)view.findViewById(R.id.txtPointDescription);
         TextView longitude =(TextView)view.findViewById(R.id.txtPointLongitude);
         TextView latitude =(TextView)view.findViewById(R.id.txtPointLatitude);
          
         index.setText(String.valueOf(points.index + 1));
         add_time.setText("Dodana: " + String.valueOf(points.getAddTime()));
         if(points.description.length() > 0)
        	 description.setText("O to�ki: " + points.description.toString());
         else
        	 description.setText("O to�ki: /");
         longitude.setText("Lng: " + String.valueOf(points.getLongitude()));
         latitude.setText("Lat: " + String.valueOf(points.getLatitude()));
	      
	     return view;
    }
    
    @Override
    public int getChildrenCount(int groupPosition) {
      
	     ArrayList<Points> productList = deptList.get(groupPosition).getPoints();
	     return productList.size();
    
    }
    
    @Override
    public Object getGroup(int groupPosition) {
    	return deptList.get(groupPosition);
    }
    
    @Override
    public int getGroupCount() {
    	return deptList.size();
    }
    
    @Override
    public long getGroupId(int groupPosition) {
    	return groupPosition;
    }
    
    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
      ViewGroup parent) {
      
    	Route route = (Route) getGroup(groupPosition);
    	if (view == null) {
    	    LayoutInflater inf = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    	    view = inf.inflate(R.layout.listitem, null);
    	}

    	TextView name =(TextView)view.findViewById(R.id.txtRouteName);
    	TextView description =(TextView)view.findViewById(R.id.txtRouteDescription);
    	TextView add_time =(TextView)view.findViewById(R.id.txtRouteAddTime);
    	//TextView routeNumberOfPoints =(TextView)view.findViewById(R.id.txtRouteNumberOfPoints);
    	Button btnGo = (Button) view.findViewById(R.id.btn_go);
    	GraphView graph = (GraphView)view.findViewById(R.id.graph);

    	boolean[] marks = new boolean[route.points.size()];
    	
    	for(int i = 0; i < route.points.size(); i++)
    	{
    		Points p = route.points.get(i);
    		marks[i] = p.visited;
    	}
    	graph.setMarks(marks);

    	name.setText(route.name.toString());
    	description.setText("Opis: " + route.description.toString());
    	add_time.setText("Dodana: " + String.valueOf(route.getAddTime()));
    	//routeNumberOfPoints.setText("�t. postojank: " + String.valueOf(route.points.size()));

    	btnGo.setTag(groupPosition);
    	btnGo.setOnClickListener(new Button.OnClickListener(){
    	    public void onClick(View v)
    	    {
    	        Intent i = new Intent(context, MapActivity.class);
    	        i.putExtra("Data", (Integer) v.getTag());
    	        context.startActivity(i);
    	    }
    	});
    	return view;
	}
    
    
    @Override
    public boolean hasStableIds() {
    	return true;
    }
    
    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
    	return true;
    }
}

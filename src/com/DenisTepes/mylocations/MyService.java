package com.DenisTepes.mylocations;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.LocationSource.OnLocationChangedListener;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.os.Vibrator;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

public class MyService extends Service  implements LocationListener, LocationSource{
	
	private ApplicationData app;

    private OnLocationChangedListener mListener;
    private LocationManager locationManager;

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
    public void onCreate() {

        mgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
		app = (ApplicationData) getApplication();
		int value = app.value;
		
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, this);
		 
        if(locationManager != null)
        {
            boolean gpsIsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkIsEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
             
            if(gpsIsEnabled)
            {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000L, 10F, this);
            }
            else if(networkIsEnabled)
            {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000L, 10F, this);
            }
            else
            {
                //Show an error dialog that GPS is disabled...
            }
        }
        else
        {
            //Show some generic error dialog because something must have gone wrong with location manager.
        }
		
		setUpMapIfNeeded(value);
    }
	
	private void setUpMapIfNeeded(int routeIndex) {

        
    }
 
    @Override
    public void onStart(Intent intent, int startId) {
    }
 
    @Override
    public void onDestroy() {
    }

	@Override
	public void activate(OnLocationChangedListener listener) {
        mListener = listener;
		
	}

	@Override
	public void deactivate() {
        mListener = null;
		
	}

	@Override
	public void onLocationChanged(Location location) {
		
		try{
		    new ApiCallUpdateLocation().execute(app.user.username, app.user.password,
					String.valueOf(location.getLongitude()),
					String.valueOf(location.getLatitude()),
					String.valueOf(location.getAccuracy()));
		    
			if(app.route != null)
			{
				LatLng ll = null;
				for(int i = 0; i < app.route.points.size(); i++)
				{
					if(app.route.points.get(i).visited == false)
					{
						ll = app.route.points.get(i).getPosition();
						
						float[] results = new float[1];
						Location.distanceBetween(location.getLatitude(), location.getLongitude(),
										 ll.latitude, ll.longitude, results);
						
						if(results[0] < 20)
							new ApiCallVisited().execute(app.user.username, app.user.password,
														 String.valueOf(app.route.points.get(i).id));
							
						//Toast.makeText(this, String.valueOf(results[0]), Toast.LENGTH_SHORT).show();
						//break za upo�tevanje vrstnega reda
					}
				}
			}
		}catch(Exception ee)
		{
		}
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}
    private static final int NOTIFY_ME_ID = 1987;
 
    private NotificationManager mgr = null;
	
	public void createNotification(int index)
	{
		Points p = app.route.points.get(index);
		NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
         
        int icon = R.drawable.ic_launcher;
        CharSequence text = "To�ka je obiskana";
        CharSequence contentTitle = (p.index + 1) + ". to�ka je obiskana";
        CharSequence contentText = p.description.length() > 0 ? p.description: "Ni opisa to�ke";
        long when = System.currentTimeMillis();
         
        Intent intent = new Intent(this, MapActivity.class);
        intent.removeExtra("FocusedPoint");
        intent.setData((Uri.parse(String.valueOf(p.index))));
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.setFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
        intent.setFlags(PendingIntent.FLAG_CANCEL_CURRENT);
        PendingIntent contentIntent = PendingIntent.getActivity(this, 0, intent, 0);
        Notification notification = new Notification(icon,text,when);


        notification.ledARGB = Color.RED;
        notification.ledOffMS = 300;
        notification.ledOnMS = 300;
         
        notification.defaults |= Notification.DEFAULT_LIGHTS;
        //notification.flags |= Notification.FLAG_SHOW_LIGHTS;
         
        notification.setLatestEventInfo(this, contentTitle, contentText, contentIntent);
         
        notificationManager.notify(app.setNotificationID(p.index), notification);
	}
	
private class ApiCallUpdateLocation extends AsyncTask<String, Void, Boolean>{
	
    @Override
    protected Boolean doInBackground(String... da) {
    	try{
    		String data = "/" + da[2] + "/" + da[3] + "/" + da[4];
			HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/update-my-location" + data).openConnection();
			conn.setUseCaches(false);
			conn.setDoOutput(true);
		   
			conn.setRequestProperty("Authorization", "Basic " +
		           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));

			conn.connect();
		   
			int response = conn.getResponseCode();
			
			data="";
			if (response == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = conn.getInputStream();
   
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(inputStream));
				String temp;
				while ((temp = bufferedReader.readLine()) != null) {
					data+=temp;
				}
			}
			
			System.out.println(data);
			
			return true;
			
		}
		catch (IOException i){
		}
    	
    	return false;
    }
    
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(Boolean result) {
    	if(result = false)
    	{
			Toast.makeText(getApplicationContext(),
	    			"Te�ave pri posodabljanju lokacije",
					Toast.LENGTH_SHORT).show();
    	}
   }
}

private class ApiCallGetRoutePointsData extends AsyncTask<String, Void, Boolean>{

    @Override
    protected Boolean doInBackground(String... da) {
    	try{
    		String data = "/" + da[2];
			HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/get-route-points-data" + data).openConnection();
			conn.setUseCaches(false);
			conn.setDoOutput(true);
		   
			conn.setRequestProperty("Authorization", "Basic " +
		           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));

			conn.connect();
		   
			int response = conn.getResponseCode();
			
			data="";
			if (response == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = conn.getInputStream();
   
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(inputStream));
				String temp;
				while ((temp = bufferedReader.readLine()) != null) {
					data+=temp;
				}
			}
			
			Gson gson = new GsonBuilder().create();
			ArrayList<PointData> xxx = (ArrayList<PointData>)gson.fromJson(data, new TypeToken<ArrayList<PointData>>(){}.getType());

			for(int i = 0; i < xxx.size(); i++)
			{
				app.route.setVisitedPoint(xxx.get(i).pointid);
			}
			
			return true;
			
		}
		catch (IOException i){
		}
    	
    	return false;
    }
    
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(Boolean result) {
    	if(result = false)
    	{
        		Toast.makeText(getApplicationContext(),
	        			"Nalaganje poti NE uspe�no!",
						Toast.LENGTH_SHORT).show();

    	}else{
        	Toast.makeText(getApplicationContext(),
        			"Nalaganje poti uspe�no!",
					Toast.LENGTH_SHORT).show();
    	}

   }
}

private class ApiCallVisited extends AsyncTask<String, Void, Integer>{
	
    @Override
    protected Integer doInBackground(String... da) {
    	try{
    		String data = "/" + da[2];
			HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/visit-routepoint" + data).openConnection();
			conn.setUseCaches(false);
			conn.setDoOutput(true);
		   
			conn.setRequestProperty("Authorization", "Basic " +
		           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));

			conn.connect();
		   
			int response = conn.getResponseCode();
			
			data="";
			if (response == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = conn.getInputStream();
   
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(inputStream));
				String temp;
				while ((temp = bufferedReader.readLine()) != null) {
					data+=temp;
				}
			}
			if(data.toLowerCase().contains("result") && data.toLowerCase().contains("true"))
			{
				app.route.setVisitedPoint(Integer.parseInt(da[2]));
				for(int i = 0; i < app.route.points.size(); i++)
					if(app.route.points.get(i).id == Integer.parseInt(da[2]))
						return i;
			}
			
			return -1;
			
		}
		catch (IOException i){
		}
    	
    	return -1;
    }
    
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(Integer result) {
    	if(result == -1)
    	{
        		Toast.makeText(getApplicationContext(),
	        			"Napaka pri obisku poti ali pa je �e bila obiskana!",
						Toast.LENGTH_SHORT).show();

    	}else{
    		// Get instance of Vibrator from current Context
    		Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
    		 
    		// Vibrate for 300 milliseconds
    		v.vibrate(300);
    		
    		createNotification(result);

    	}

   }
}
}


package com.DenisTepes.mylocations;

import java.sql.Date;
import java.text.SimpleDateFormat;

import android.test.suitebuilder.annotation.LargeTest;
import android.text.format.DateFormat;
import android.text.method.DateTimeKeyListener;

import com.google.android.gms.maps.model.LatLng;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Points {
	@Expose
	@SerializedName("id")
	public int id;
	@Expose
	@SerializedName("index")
	public int index;
	@Expose
	@SerializedName("routeid")
	public int routeid;
	@Expose
	@SerializedName("description")
	public String description;
	@Expose
	@SerializedName("add_time")
	public long add_time;
	@Expose
	@SerializedName("longitude")
	public double longitude;
	@Expose
	@SerializedName("latitude")
	public double latitude;
	@Expose
	@SerializedName("visited")
	public boolean visited = false;
	
	public Points() {
		super();
	}
	
	public Points(int id, int index, int routeid, 
			String description, long add_time, double longitude, double latitude) {
		super();
		this.id = id;
		this.index = index;
		this.routeid = routeid;
		this.description = description;
		this.add_time = add_time;
		this.longitude = longitude;
		this.latitude = latitude;
	}
	
	public String getAddTime(){
		SimpleDateFormat df = new SimpleDateFormat("dd.MM.yyyy HH:mm");
		Date dt = new Date(add_time * 1000);
		return df.format(dt);
	}
	
	public String getLongitude(){
		int d = (int) longitude;  // Truncate the decimals
		int m = (int) ((longitude - d) * 60);
		int s = (int) ((((longitude - d) * 60) - m) * 60);
		return String.valueOf(d) + "�" + String.valueOf(m) + "'" + String.valueOf(s) + "\"";
	}
	
	public String getLatitude(){
		int d = (int) latitude;  // Truncate the decimals
		int m = (int) ((latitude - d) * 60);
		int s = (int) ((((latitude - d) * 60) - m) * 60);
		return String.valueOf(d) + "�" + String.valueOf(m) + "'" + String.valueOf(s) + "\"";
	}
	
	public LatLng getPosition(){
		return new LatLng(latitude, longitude);
	}
}

package com.DenisTepes.mylocations;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.text.DateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpConnection;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.UiSettings;
import com.google.android.gms.maps.GoogleMap.*;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Vibrator;
import android.R.integer;
import android.R.string;
import android.app.Activity;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Camera;
import android.support.v4.app.FragmentActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.Toast;

public class MapActivity extends FragmentActivity implements LocationListener, LocationSource
{
	public Context context = this;
	public Dialog dialog = null;
	private ProgressDialog progressDialog;
	
	private GoogleMap mMap;

    private OnLocationChangedListener mListener;
    private LocationManager locationManager;
    
	private ApplicationData app;
	public ArrayList<Marker> markers;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_map);
		
		app = (ApplicationData) getApplication();
		
        if (getIntent().hasExtra("Data")) {
			int value = getIntent().getExtras().getInt("Data");
	        app.value = value;
        }

        app.route = new Route();
        app.route = app.routes.get(app.value);
		
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000, 0, this);
		 
        if(locationManager != null)
        {
            boolean gpsIsEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
            boolean networkIsEnabled = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
             
            if(gpsIsEnabled)
            {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 5000L, 10F, this);
            }
            else if(networkIsEnabled)
            {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 5000L, 10F, this);
            }
            else
            {
                //Show an error dialog that GPS is disabled...
            }
        }
        else
        {
            //Show some generic error dialog because something must have gone wrong with location manager.
        }
		
        ImageView btn = (ImageView)findViewById(R.id.btnShare);
        btn.setOnClickListener(new View.OnClickListener() {
	        public void onClick(View v){
	        	// custom dialog
				dialog = new Dialog(context);
				dialog.setContentView(R.layout.share);
				dialog.setTitle("Deli");
	 
	 
				Button dialogButton = (Button) dialog.findViewById(R.id.btnDeli);
				// if button is clicked, close the custom dialog
				dialogButton.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View v) {
						EditText txtUser = (EditText)dialog.findViewById(R.id.txtDeliUsername);
						EditText txtKomentar = (EditText)dialog.findViewById(R.id.txtDeliKomentar);

						String komentar = txtKomentar.getText().toString();
						komentar = komentar.replaceAll(" ", "%20");
						dialog.dismiss();
						new ApiCallShare().execute(app.user.username, app.user.password,
												   txtUser.getText().toString(),
												   String.valueOf(app.route.id),
												   komentar);
					}
				});
	 
				dialog.show();
	        	}
	       });
        
		setUpMapIfNeeded(app.value);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}


	private void setUpMapIfNeeded(int routeIndex) {
        if (mMap == null) {
            mMap = ((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map))
                    .getMap();
            
            if (mMap != null) 
            {
            	updateMyLocation();
            }

            stopService(new Intent(this, MyService.class));
            
            mMap.getMyLocation();
            
            setMarkers();

            if (getIntent().getData() != null)
            {
            	String data = getIntent().getData().toString();
    			int i = Integer.parseInt(data);
    			System.out.println("focusedPoint: " + String.valueOf(i));

    			try{
    		        NotificationManager notificationManager = (NotificationManager)getSystemService(Context.NOTIFICATION_SERVICE);
    		        notificationManager.cancel(app.getNotificationID(i));
    			}
    			catch(Exception ee){}
    			
            	markers.get(i).showInfoWindow();
            	CameraPosition cp = new CameraPosition.Builder()
	             .target(markers.get(i).getPosition())
	             .zoom(15)
	             .build();     
            	mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
            }else{
            	markers.get(0).showInfoWindow();
            	CameraPosition cp = new CameraPosition.Builder()
	             .target(markers.get(0).getPosition())
	             .zoom(15)
	             .build();     
            	mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
            }
            //new ApiCallGetRoutePointsData().execute(app.user.username, app.user.password,
			//										String.valueOf(app.route.id));
        }
    }

	private boolean checkReady() {
        if (mMap == null) {
            Toast.makeText(this, "Tezave pri prikazu", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
	
	private void updateMyLocation() {
        if (!checkReady()) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
    }

	@Override
    public void onPause()
    {
        if(locationManager != null)
        {
            locationManager.removeUpdates(this);
        }
        startService(new Intent(this, MyService.class));
        
        super.onPause();
    }
     
    @Override
    public void onResume()
    {
        super.onResume();
        
        setUpMapIfNeeded(app.value);
         
        if(locationManager != null)
        {
            mMap.setMyLocationEnabled(true);
        }
    }
	
	@Override
	public void onLocationChanged(Location location) 
	{
		try{
		    if( mListener != null )
		    {
		        mListener.onLocationChanged( location );
		 
		        LatLngBounds bounds = this.mMap.getProjection().getVisibleRegion().latLngBounds;
		 
		        if(!bounds.contains(new LatLng(location.getLatitude(), location.getLongitude())))
		        {
		             //Move the camera to the user's location once it's available!
		             //mMap.animateCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(), location.getLongitude())));
		             CameraPosition cp = new CameraPosition.Builder()
		             .target(new LatLng(location.getLatitude(),location.getLongitude()))
		             .zoom(15)
		             .build();     
		             mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
		        }
		    }
		    new ApiCallUpdateLocation().execute(app.user.username, app.user.password,
					String.valueOf(location.getLongitude()),
					String.valueOf(location.getLatitude()),
					String.valueOf(location.getAccuracy()));
		    
			if(app.route != null)
			{
				LatLng ll = null;
				for(int i = 0; i < app.route.points.size(); i++)
				{
					if(app.route.points.get(i).visited == false)
					{
						ll = app.route.points.get(i).getPosition();
						
						float[] results = new float[1];
						Location.distanceBetween(location.getLatitude(), location.getLongitude(),
										 ll.latitude, ll.longitude, results);
						
						if(results[0] < 20) // distance from point
							new ApiCallVisited().execute(app.user.username, app.user.password,
														 String.valueOf(app.route.points.get(i).id));
							
						//Toast.makeText(this, String.valueOf(results[0]), Toast.LENGTH_SHORT).show();
						//break; //break za upo�tevanje vrstnega reda
					}
				}
			}
		}catch(Exception ee)
		{}
	}
	
	@Override
    public void activate(OnLocationChangedListener listener) 
    {
        mListener = listener;
        Toast.makeText(this, "active", Toast.LENGTH_SHORT).show();
    }
     
    @Override
    public void deactivate() 
    {
        mListener = null;
    }
 
    @Override
    public void onProviderDisabled(String provider) 
    {
        // TODO Auto-generated method stub
        Toast.makeText(this, "provider disabled", Toast.LENGTH_SHORT).show();
    }
 
    @Override
    public void onProviderEnabled(String provider) 
    {
        try{
	        Location location = locationManager.getLastKnownLocation(provider);
	        CameraPosition cp = new CameraPosition.Builder()
	        .target(new LatLng(location.getLatitude(),location.getLongitude()))
	        .zoom(17)
	        .build();
	        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
	        mMap.getUiSettings().setZoomControlsEnabled(true);
	        mMap.getUiSettings().setCompassEnabled(true);
        }
        catch(Exception ee)
        {}
    }
 
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) 
    {
        try{
	        Location location = locationManager.getLastKnownLocation(provider);
	        CameraPosition cp = new CameraPosition.Builder()
	        .target(new LatLng(location.getLatitude(),location.getLongitude()))
	        .zoom(17)
	        .build();
	        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));
	        mMap.getUiSettings().setZoomControlsEnabled(true);
	        mMap.getUiSettings().setCompassEnabled(true);
        }
        catch(Exception ee)
        {}
    }
    
    public void setMarkers()
    {
    	markers = new ArrayList<Marker>();
        
        for(int i = 0; i < app.route.points.size(); i++)
        {
        	Marker m = null;
        	if(i == 0){
            	m = mMap.addMarker(new MarkerOptions()
                .position(app.route.points.get(i).getPosition())
                .title(String.valueOf(app.route.points.get(i).index + 1))
                .snippet(app.route.points.get(i).description)
                .icon(BitmapDescriptorFactory.defaultMarker(150)));	
        	}else if(app.route.points.get(i).visited == true){
            	m = mMap.addMarker(new MarkerOptions()
                .position(app.route.points.get(i).getPosition())
                .title(String.valueOf(app.route.points.get(i).index + 1))
                .snippet(app.route.points.get(i).description)
                .icon(BitmapDescriptorFactory.defaultMarker(90)));
        	}else{
            	m = mMap.addMarker(new MarkerOptions()
                .position(app.route.points.get(i).getPosition())
                .title(String.valueOf(app.route.points.get(i).index + 1))
                .snippet(app.route.points.get(i).description));
        	}
        	markers.add(m);
        }
    }
    
private class ApiCallUpdateLocation extends AsyncTask<String, Void, Boolean>{
		
        @Override
        protected Boolean doInBackground(String... da) {
        	try{
        		String data = "/" + da[2] + "/" + da[3] + "/" + da[4];
				HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/update-my-location" + data).openConnection();
				conn.setUseCaches(false);
				conn.setDoOutput(true);
			   
				conn.setRequestProperty("Authorization", "Basic " +
			           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));

				conn.connect();
			   
				int response = conn.getResponseCode();
				
				data="";
				if (response == HttpURLConnection.HTTP_OK) {
					InputStream inputStream = conn.getInputStream();
	   
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(inputStream));
					String temp;
					while ((temp = bufferedReader.readLine()) != null) {
						data+=temp;
					}
				}
				
				System.out.println(data);
				
				return true;
				
			}
			catch (IOException i){
			}
        	
        	return false;
        }
        
        // onPostExecute displays the results of the AsyncTask.
        @Override
        protected void onPostExecute(Boolean result) {
        	if(result = false)
        	{
	    		Toast.makeText(getApplicationContext(),
	        			"Te�ave pri posodabljanju lokacije",
						Toast.LENGTH_SHORT).show();
        	}//else
    		    //Toast.makeText(getApplicationContext(), "lokacija posodobljena", Toast.LENGTH_SHORT).show();

       }
    }

private class ApiCallGetRoutePointsData extends AsyncTask<String, Void, Boolean>{
	
	    @Override
	    protected Boolean doInBackground(String... da) {
	    	try{
	    		String data = "/" + da[2];
				HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/get-route-points-data" + data).openConnection();
				conn.setUseCaches(false);
				conn.setDoOutput(true);
			   
				conn.setRequestProperty("Authorization", "Basic " +
			           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));
	
				conn.connect();
			   
				int response = conn.getResponseCode();
				
				data="";
				if (response == HttpURLConnection.HTTP_OK) {
					InputStream inputStream = conn.getInputStream();
	   
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(inputStream));
					String temp;
					while ((temp = bufferedReader.readLine()) != null) {
						data+=temp;
					}
				}
				
				Gson gson = new GsonBuilder().create();
				ArrayList<PointData> xxx = (ArrayList<PointData>)gson.fromJson(data, new TypeToken<ArrayList<PointData>>(){}.getType());

				for(int i = 0; i < xxx.size(); i++)
				{
					app.route.setVisitedPoint(xxx.get(i).pointid);
				}
				
				return true;
				
			}
			catch (IOException i){
			}
	    	
	    	return false;
	    }
	    
	    // onPostExecute displays the results of the AsyncTask.
	    @Override
	    protected void onPostExecute(Boolean result) {
	    	if(result = false)
	    	{
	        		Toast.makeText(getApplicationContext(),
		        			"Nalaganje poti NE uspe�no!",
							Toast.LENGTH_SHORT).show();
	
	    	}else{
	    		markers = new ArrayList<Marker>();
	            
	            for(int i = 0; i < app.route.points.size(); i++)
	            {
	            	Marker m = null;
	            	if(i == 0){
	                	m = mMap.addMarker(new MarkerOptions()
	                    .position(app.route.points.get(i).getPosition())
	                    .title(String.valueOf(app.route.points.get(i).index + 1))
	                    .snippet(app.route.points.get(i).description)
	                    .icon(BitmapDescriptorFactory.defaultMarker(150)));	
	            	}else if(app.route.points.get(i).visited == true){
		            	m = mMap.addMarker(new MarkerOptions()
		                .position(app.route.points.get(i).getPosition())
		                .title(String.valueOf(app.route.points.get(i).index + 1))
		                .snippet(app.route.points.get(i).description)
	                    .icon(BitmapDescriptorFactory.defaultMarker(90)));
	            	}else{
		            	m = mMap.addMarker(new MarkerOptions()
		                .position(app.route.points.get(i).getPosition())
		                .title(String.valueOf(app.route.points.get(i).index + 1))
		                .snippet(app.route.points.get(i).description));
	            	}
	            	markers.add(m);
	            }
	    	}
	
	   }
	}

private class ApiCallVisited extends AsyncTask<String, Void, Integer>{
		
	    @Override
	    protected Integer doInBackground(String... da) {
	    	try{
	    		String data = "/" + da[2];
				HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/visit-routepoint" + data).openConnection();
				conn.setUseCaches(false);
				conn.setDoOutput(true);
			   
				conn.setRequestProperty("Authorization", "Basic " +
			           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));
	
				conn.connect();
			   
				int response = conn.getResponseCode();
				
				data="";
				if (response == HttpURLConnection.HTTP_OK) {
					InputStream inputStream = conn.getInputStream();
	   
					BufferedReader bufferedReader = new BufferedReader(
							new InputStreamReader(inputStream));
					String temp;
					while ((temp = bufferedReader.readLine()) != null) {
						data+=temp;
					}
				}
				
				if(data.toLowerCase().contains("result") && data.toLowerCase().contains("true"))
				{
					app.route.setVisitedPoint(Integer.parseInt(da[2]));
					for(int i = 0; i < app.route.points.size(); i++)
						if(app.route.points.get(i).id == Integer.parseInt(da[2]))
							return i;
				}
				
				return -1;
				
			}
			catch (IOException i){
			}
	    	
	    	return -1;
	    }
	    
	    // onPostExecute displays the results of the AsyncTask.
	    @Override
	    protected void onPostExecute(Integer result) {
	    	if(result == -1)
	    	{
	        		Toast.makeText(getApplicationContext(),
		        			"Napaka pri obisku poti ali pa je �e bila obiskana",
							Toast.LENGTH_SHORT).show();
	
	    	}else{
				LatLng ll = markers.get(result).getPosition();
				String snippet = markers.get(result).getSnippet();
				String title = markers.get(result).getTitle();
				Marker m = markers.get(result);
				m.remove();
				
				markers.set(result, mMap.addMarker(new MarkerOptions()
                .position(ll)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.defaultMarker(90))));
				
				// Get instance of Vibrator from current Context
				Vibrator v = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
				 
				// Vibrate for 300 milliseconds
				v.vibrate(300);
				
				m.showInfoWindow();
            	CameraPosition cp = new CameraPosition.Builder()
	             .target(m.getPosition())
	             .zoom(15)
	             .build();     
            	mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cp));

	    	}
	
	   }
	}

private class ApiCallShare extends AsyncTask<String, String, String>{
	
    @Override
    protected String doInBackground(String... da) {
    	try{
    		String data = "/" + da[2] + "/" + da[3] + "/" + da[4];
			HttpURLConnection conn = (HttpURLConnection) new URL(app.ipaddress + "/api/map/share-route" + data).openConnection();
			conn.setUseCaches(false);
		   
			conn.setRequestProperty("Authorization", "Basic " +
		           Base64.encodeToString((da[0]+":"+da[1]).getBytes(), Base64.DEFAULT));
		   
			conn.connect();
		   
			int response = conn.getResponseCode();
			
			data="";
			if (response == HttpURLConnection.HTTP_OK) {
				InputStream inputStream = conn.getInputStream();
   
				BufferedReader bufferedReader = new BufferedReader(
						new InputStreamReader(inputStream));
				String temp;
				while ((temp = bufferedReader.readLine()) != null) {
					data+=temp;
				}
			}
			
			System.out.println(data);
			
			return data;
			
		}
		catch (IOException i){
		}
    	
    	return null;
    }
    
    @Override  
    protected void onPreExecute()  
    {  
    	progressDialog = ProgressDialog.show(MapActivity.this,"Nalaganje...",  
    		    "Po�iljanje zahteve za deljenje poti, prosim po�akajte...", false, false);
        //Display the progress dialog  
        progressDialog.show();  
    }  
    
    // onPostExecute displays the results of the AsyncTask.
    @Override
    protected void onPostExecute(String result) {
    	
    	progressDialog.dismiss();
    	
    	if(result != null)
    	{
    		if(result.toLowerCase().contains("route doesnt exist"))
			{
    			Toast.makeText(getApplicationContext(),
            			"Napaka pri po�iljanju poti!",
    					Toast.LENGTH_SHORT).show();
    			dialog.show();
			}
    		else if(result.toLowerCase().contains("user doesnt exist"))
			{
				Toast.makeText(getApplicationContext(),
	        			"Uporabnik ne obstaja!",
						Toast.LENGTH_SHORT).show();
    			dialog.show();
			}
    		else if(result.toLowerCase().contains("user already has this route"))
			{
				Toast.makeText(getApplicationContext(),
	        			"Uporabnik �e ima to pot!",
						Toast.LENGTH_SHORT).show();
			}
    		else
			{
				Toast.makeText(getApplicationContext(),
	        			"Deljenje je bilo uspe�no!",
						Toast.LENGTH_SHORT).show();
			}		
    	}
    	else
    	{
        	Toast.makeText(getApplicationContext(),
        			"Napaka pri povezavi!",
					Toast.LENGTH_SHORT).show();
			dialog.show();
    	}
   }
}
}
